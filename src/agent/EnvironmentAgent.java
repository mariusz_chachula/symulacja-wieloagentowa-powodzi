package agent;

import java.util.HashSet;
import java.util.Set;

import behaviour.EnvironmentReceiveBehaviour;
import behaviour.EnvironmentRegisterAgents;
import behaviour.EnvironmentSendBehaviour;
import data.Environment;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;

public class EnvironmentAgent extends Agent {

	private static final long serialVersionUID = -422150392926075703L;

	private Environment environment;

	private Set<AID> persons;

	private boolean started;

	protected void setup() {
		System.out.println("Environment " + getAID().getName() + " is ready.");
		Object[] args = getArguments();
		if (args == null || args.length < 1) {
			return;
		}

		register();
		addBehaviour(new EnvironmentRegisterAgents(Integer.parseInt((String) args[0])));
		persons = new HashSet<>();
		environment = loadEnviroment();
	}

	private Environment loadEnviroment() {
		// TODO ZALADOWAC SRODOWISKO
		return null;
	}

	private void register() {
		try {
			DFService.register(this, new DFAgentDescription());
		} catch (FIPAException e) {
		}
	}

	public void startSimulation() {
		started = true;
		addBehaviour(new EnvironmentReceiveBehaviour());
		addBehaviour(new EnvironmentSendBehaviour());
	}

	@Override
	protected void takeDown() {
		System.out.println("Agent " + getAID().getName() + " gonna die!");
		try {
			DFService.deregister(this);
		} catch (FIPAException e) {
		}
	}

	public void addPersonAgent(AID agent) {
		persons.add(agent);
	}

	public Set<AID> getPersonAgents() {
		return persons;
	}
}