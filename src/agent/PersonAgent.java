package agent;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import behaviour.PersonMoveBehaviour;
import behaviour.PersonReceiveBehaviour;
import behaviour.PersonRegisterBehaviour;
import behaviour.PersonSendBehaviour;
import jade.core.AID;
import jade.core.Agent;

public class PersonAgent extends Agent {

	private static final long serialVersionUID = -422150392926075703L;

	private Set<AID> agentsInArea = new HashSet<>();

	private AID enviromentAID;

	private boolean started = false;

	protected void setup() {
		System.out.println("Hello! Agent " + getAID().getName() + " is ready.");
		Object[] args = getArguments();
		if (args != null && args.length > 0) {
			System.out.println(args[0]);
		}
		addBehaviour(new PersonRegisterBehaviour());
	}

	public void startListening() {
		addBehaviour(new PersonReceiveBehaviour());
	}

	public void startSimulation() {
		started = true;
		addBehaviour(new PersonSendBehaviour());
		addBehaviour(new PersonMoveBehaviour());
	}

	@Override
	protected void takeDown() {
		System.out.println("Agent " + getAID().getName() + " gonna die!");
		started = false;
	}

	public Collection<AID> getAgentsInArea() {
		return agentsInArea;
	}

	public void addAgentInArea(AID agent) {
		agentsInArea.add(agent);
	}

	public void setEnviromentAID(AID env) {
		enviromentAID = env;
	}

	public AID getEnviromentAID() {
		return enviromentAID;
	}

	public boolean isStarted() {
		return started;
	}

}