package data;

import java.util.ArrayList;
import java.util.List;

public class Grid {

	List<List<Cell>> grid = new ArrayList<>();

	public Grid(int x, int y) {
		initializeList(x, y);
	}

	private void initializeList(int x, int y) {
		for (int i = 0; i < x; i++) {
			List<Cell> row = new ArrayList<>();
			for (int j = 0; j < y; j++) {
				row.add(randomCell(i, j));
			}
			grid.add(row);
		}
	}

	public List<List<Cell>> nearbyEnvironment(int x, int y, int range) {
		return null;
	}

	private Cell randomCell(int i, int j) {
		Cell cell = new Cell(i, j, 2, 0, new Type());
		return cell;
	}
}
