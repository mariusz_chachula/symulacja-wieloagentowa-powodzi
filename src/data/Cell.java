package data;

public class Cell {
	private int x;
	private int y;
	private int z;
	private int waterLevel;
	Type type;

	public Cell(int x, int y, int z, int waterLevel, Type type) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
		this.waterLevel = waterLevel;
		this.type = type;
	}

	public int getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(int waterLevel) {
		this.waterLevel = waterLevel;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public Type getType() {
		return type;
	}
}
