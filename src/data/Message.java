package data;

import jade.lang.acl.ACLMessage;

public class Message extends ACLMessage {

	private static final long serialVersionUID = 1351640145867953397L;

	private Object data;

	public Message(int type) {
		super(type);
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
