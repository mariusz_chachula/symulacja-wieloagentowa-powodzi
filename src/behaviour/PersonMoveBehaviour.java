package behaviour;

import agent.PersonAgent;
import data.Message;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class PersonMoveBehaviour extends Behaviour {

	private static final long serialVersionUID = 8833632348170757371L;

	private boolean finished = false;

	private int step = 0;

	@Override
	public void action() {
		step++;
		try {
			// TODO napsiac algo ktory wybiera gdzie sie rusza osoba na
			// podstawie posiadanych danych (srodowisko i dane od innych
			// agnetow)

			Message msg = new Message(ACLMessage.INFORM);
			msg.addReceiver(((PersonAgent) getAgent()).getEnviromentAID());

			msg.setData("Ruszam sie " + step + " Agent: " + getAgent().getLocalName());
			System.out.println(getAgent().getAID().getLocalName() + " wysyla info o ruchu do env " + step + " raz");
			getAgent().send(msg);

			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
	}

	@Override
	public boolean done() {
		return finished;
	}
}
