package behaviour;

import agent.PersonAgent;
import data.Message;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;

public class PersonRegisterBehaviour extends Behaviour {

	private static final long serialVersionUID = 8358978125971453314L;

	private boolean registered = false;

	@Override
	public void action() {
		try {
			DFAgentDescription template = new DFAgentDescription();
			DFAgentDescription[] results;
			results = DFService.search(myAgent, template);
			Message msg = new Message(ACLMessage.INFORM);
			if (results.length != 0) {
				((PersonAgent) getAgent()).setEnviromentAID(results[0].getName());
				msg.addReceiver(((PersonAgent) getAgent()).getEnviromentAID());
				msg.setData(getAgent().getAID());
				System.out.println(getAgent().getAID().getLocalName() + " rejestruje sie u env");
				getAgent().send(msg);
				registered = true;
			}
		} catch (FIPAException e) {
		}
	}

	@Override
	public boolean done() {
		if (registered) {
			((PersonAgent) getAgent()).startListening();
		}
		return registered;
	}
}
