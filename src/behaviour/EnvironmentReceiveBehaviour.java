package behaviour;

import agent.EnvironmentAgent;
import data.Message;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class EnvironmentReceiveBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 8391849111645987135L;

	private int step = 0;

	@Override
	public void action() {
		ACLMessage msg;
		while ((msg = getAgent().receive()) != null) {
			if (msg instanceof Message) {
				step++;
				System.out.println(getAgent().getAID().getLocalName() + " odbiera wiadomosc od agenta "
						+ ((Message) msg).getSender().getLocalName() + " o tresci " + ((Message) msg).getData());
				doSomething((Message) msg);
			} else {
				block();
			}
		}
	}

	private void doSomething(Message recivedMsg) {
		// TODO: wysylac odpowiednie dane o updatowanym srodowisku do wszystkich
		// agentow

		Message reply = new Message(ACLMessage.INFORM);
		for (AID agent : ((EnvironmentAgent) getAgent()).getPersonAgents()) {
			reply.addReceiver(agent);
		}

		if (reply.getAllIntendedReceiver().hasNext()) {
			reply.setData("NEW ENV DATA " + step + " FROM " + getAgent().getLocalName());
			System.out.println(
					getAgent().getAID().getLocalName() + " wysyla wiadomosc updatujaca srodowisko " + step + " raz");
			getAgent().send(reply);
		}
	}
}
