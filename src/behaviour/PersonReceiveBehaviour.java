package behaviour;

import agent.PersonAgent;
import data.Message;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class PersonReceiveBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 8391849111645987135L;

	@Override
	public void action() {
		ACLMessage msg;
		while ((msg = getAgent().receive()) != null) {
			doSomething((Message) msg);
		}
		block();
	}

	private void doSomething(Message msg) {
		if (msg.getSender().equals(((PersonAgent) getAgent()).getEnviromentAID())) {
			// TODO wczytywac srodowisko i inne dane od agenta srodowisko. Ify
			// do zmiany
			if (!((PersonAgent) getAgent()).isStarted()) {
				((PersonAgent) getAgent()).startSimulation();
				System.out.println(getAgent().getAID().getLocalName() + " startuje symulacje");
			} else {
				System.out.println(getAgent().getAID().getLocalName() + " odbiera wiadomosc od env: " + msg.getData());
			}
		}
	}
}
