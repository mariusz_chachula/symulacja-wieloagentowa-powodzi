package behaviour;

import agent.EnvironmentAgent;
import data.Message;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class EnvironmentRegisterAgents extends Behaviour {

	private static final long serialVersionUID = 4913112594016096868L;

	private int agentsToRegister = 0;

	public EnvironmentRegisterAgents(int agentsToRegister) {
		this.agentsToRegister = agentsToRegister;
	}

	@Override
	public void action() {
		ACLMessage msg = getAgent().receive();
		if (msg != null) {
			System.out
					.println(getAgent().getAID().getLocalName() + " zarejestrowal agenta " + ((Message) msg).getData());
			if (((Message) msg).getData() instanceof AID) {
				((EnvironmentAgent) getAgent()).addPersonAgent((AID) ((Message) msg).getData());
			}
		} else {
			block();
		}
	}

	@Override
	public boolean done() {
		if (((EnvironmentAgent) getAgent()).getPersonAgents().size() == agentsToRegister) {
			((EnvironmentAgent) getAgent()).startSimulation();
			return true;
		}
		return false;
	}
}
