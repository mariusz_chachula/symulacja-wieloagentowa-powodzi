package behaviour;

import java.util.Collection;

import agent.PersonAgent;
import data.Message;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class PersonSendBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 8391849111645987135L;

	private int step = 0;

	@Override
	public void action() {
		step++;
		Message msg = new Message(ACLMessage.INFORM);
		for (AID agent : getAgentsInArea()) {
			if (!agent.equals(getAgent().getAID())) {
				msg.addReceiver(agent);
			}
		}

		if (msg.getAllIntendedReceiver().hasNext()) {
			msg.setData("DATA " + step + " FROM " + getAgent().getLocalName());
			System.out.println(getAgent().getAID().getLocalName() + " wysyla wiadomosc do agentow w poblizu " + step + " raz");
			getAgent().send(msg);
		}
	}

	private Collection<AID> getAgentsInArea() {
		return ((PersonAgent) getAgent()).getAgentsInArea();
	}
}
