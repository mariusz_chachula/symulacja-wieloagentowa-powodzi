package behaviour;

import agent.EnvironmentAgent;
import data.Message;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class EnvironmentSendBehaviour extends Behaviour {

	private static final long serialVersionUID = 8391849111645987135L;

	@Override
	public void action() {
		Message msg = new Message(ACLMessage.INFORM);
		for (AID agent : ((EnvironmentAgent) getAgent()).getPersonAgents()) {
			msg.addReceiver(agent);
		}
		// TODO wysylac dane poczatkowe
		if (msg.getAllIntendedReceiver().hasNext()) {
			msg.setData("START DATA " + " FROM " + getAgent().getLocalName());
			System.out.println(getAgent().getAID().getLocalName() + " wysyla wiadomosc poczatkowa do wszystkich osob");
			getAgent().send(msg);
		}
	}

	@Override
	public boolean done() {
		return true;
	}
}
