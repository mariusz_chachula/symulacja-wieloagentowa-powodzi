# Użyte narzędzia #
System zaprojektowany został z wykorzystaniem frameworka JADE 4.4.0

Wizualizację symulacji wykonano z wykorzystaniem biblioteki Java FX. 

Mapy do symulacji zostały pobrane z aplikacji "Polygonal Map Generation for Games" (http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/) w formacie XML.

# Opis klas #

## Pakiet main
* Start - klasa tworzšca agentów i uruchamiajšca symulację.

## Pakiet agent ##
* EnvironmentAgent - klasa tworzaca zachowania agenta srodowisko takie jak zalewanie, wysyłanie zmian srodowiska do agentów oraz odbierajaca zmianę położenia od agentów osób
* PersonAgent - klasa tworzaca zachowania agenta osoby

## Pakiet behaviour
Zawiera zachowania agenta środowisko oraz osoby

### Zachowania srodowiska: ###
* EnvironmentFloodBehaviour - zalewanie terenu zależne od zmiennej FLOOD_SPEED
* EnvironmentReceiveBehaviour - odbieranie przemieszczenia agentów
* EnvironmentRegisterAgents - rejestracja agentów w srodowisku
* EnvironmentSendInitialDataBehaviour - przydzielenie poczštkowych współrzędnych na mapie
* EnvironmentUpdateBehaviour - wysyłanie zmiany otoczenia do agentów 

### Zachowania agentów osób: ###
* PersonMoveBehaviour - wysyłanie przemieszczenia do agenta środowisko
* PersonReceiveBehaviour - odbieranie otoczenia agenta
* PersonRegisterBehaviour - rejestracja u agenta środowisko
* PersonSendBehaviour - wysyłanie informacji do agentów w pobliżu

## Pakiet data ##
* AgentData - klasa zawierajšca informacje przekazywane od środowiska, innych agentów takie jak otoczenie, poziom zalania, miejsce położenia agenta
* Cell - pole na mapie o współrzędnych x, y i z. Dodatkowo przechowuje informacje o zalaniu, stopniu "ufności" ( agent otrzymuje informacje od innego agenta o takiej wysokości). 
* Environment - rodowisko
* Grid - teren przechowujšcy pola w listach. Dodatkowo przechowuje metody zwracajšce pobliskich agentów w otoczeniu, minimalnš, maksymalną wysokość, poziom wody. 
* Message - klasa rozszerzajšca ACLMessage, pozwalajšca przesyłać agentom pobliskie środowisko wraz z dodatkowymi informacjami
* Type - typ pola, z pliku mapy wczytywany jest atrybut okrelajšcy czy pole to woda, przeszkoda czy teren

## Pakiet settings ##
* SimulationConstants - klasa zawierajšca zmienne aplikacji opisane poniżej

## Pakiet tools ##
* SimulationUtil - klasa zawierajšca metody pomocnicze przy wywietlaniu rodowiska
* Statistics - klasa zapisujšca statystyki do plików w folderze "statistics"

```
#!csv

Przykładowa symulacja:
Nowa symulacja
Szybkosc zalewania: 0.0
Ilosc agentow: 1
Minimalna wysokosc: 0.0
Maksymalna wysokosc: 6.49
Widocznosc agentow: 5
Rozmiar mapy: 7744
Zywe agenty; Podtopione agenty, Krok symulacji, Minimalna wysokosc zywego agenta, Maksymalna wysokosc zywego agenta, Wysokosc zatopienia, Srednia wysokosc agenta
1; 0; 1; 1.14; 1.14; 0.5; 1.14
1; 0; 6; 4.26; 4.26; 0.5; 4.26
1; 0; 11; 4.26; 4.26; 0.5; 4.26
1; 0; 16; 4.26; 4.26; 0.5; 4.26
```


## Pakiet visualization ##
* EnvironmentVisualization - wizualizacja rodowiska w czasie rzeczywistym
* Xform - tworzenie modelu środowiska 

Pola zielone - teren

Pola czerwone - agenty

Pola Niebieskie - woda

# Uruchamianie aplikacji #

## Otworzenie projektu ##
Projekt został utworzony w programie Eclipse w wersji Mars. Aby go wczytać należy zaimportować folder jako istniejšcy już projekt. Następnie należy uruchomić klasę "Start".

## Uruchomienie aplikacji ##
W celu uruchomienia aplikacji należy w wierszu poleceń przejść do folderu "aplikacja" 
```
#!cmd

cd "aplikacja" 
```
Następnie należy wywołać poniższš komendę:


```
#!cmd
java -jar SystemAgentowyPowodz.jar

```


# Konfiguracja aplikacji #
Wszystkie zmienne użyte podczas symulacji znajdujš się w pliku "config.properties".

Opis zmiennych:


* DEFAULT_WATER_LEVEL - poczštkowy poziom wody
* DEFAULT_PERSON_CONFIDENCE - domyślny stopień zaufania wobec innych osób
* DEFAULT_ENV_CONFIDENCE -  domyślny stopień zaufania wobec agenta środowiska
* MAP - nazwa pliku mapy znajdujšcej się w folderze map
* FLOOD_SPEED - ilość wody zalewajšcej teren co każdy krok
* STATISTICS_FILE - plik do którego zostana zapisane statystyki w folderze statistics
* STATISTICS_STEP - krok zalewania (symulacji), co który zostanš zapisane statystyki do pliku
* AGENTS_NUMBER - ilosc agentów
* RANGE - zakres widocznosci agentów. Zakres ten wpływa na to jak daleko widzi agent i np. przy wartosci 5 agent będzie widział wszystko znajdujšce się w odległosci 5 pól od agenta. Jego zasięg będzie wtedy kwadratem o boku 11 pól, w którego srodku znajduje się agent.
* GENERATION_TYPE - typ generacji agentów. Przyjmuje dwie możliwe wartoci RANDOM oraz FILE. FILE wczytuje poczštkowe położenie agentów z pliku GENERATION_FILE a RANDOM tworzy agentów w losowych niezalanych miejscach
* GENERATION_FILE - scieżka do pliku z poczštkowym położeniem agentów. Plik powinien zostać umieszczony w folderze "agents"


Przykładowy plik:

```
#!properties
DEFAULT_WATER_LEVEL=0.5
DEFAULT_PERSON_CONFIDENCE=0.5
DEFAULT_ENV_CONFIDENCE=1.0
START_SIMULATION=START_SIMULATION
MAP=map80000.xml
FLOOD_SPEED=0.
STATISTICS_FILE=stat.csv
STATISTICS_STEP=5
AGENTS_NUMBER=1
RANGE=5
GENERATION_TYPE=FILE
GENERATION_FILE=agentsLocations.txt
```